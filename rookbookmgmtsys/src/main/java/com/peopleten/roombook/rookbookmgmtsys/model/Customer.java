package com.peopleten.roombook.rookbookmgmtsys.model;


import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
public class Customer {
	
	@Id
	@GeneratedValue
    private Long cust_id;

    public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	@NotBlank
    private String firstname;

    public Customer(Long cust_id, @NotBlank String firstname, @NotBlank String lastname,
			@NotBlank @Email String emailid, Date dob, @NotBlank @Size(min = 8, max = 10) String password) {
		super();
		this.cust_id = cust_id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.emailid = emailid;
		this.dob = dob;
		this.password = password;
	}

	@NotBlank
    private String lastname;
    
    @NotBlank
    @Email
    private String emailid;
    
    private Date dob;
    
    public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Long getCust_id() {
		return cust_id;
	}

	public void setCust_id(Long cust_id) {
		this.cust_id = cust_id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@NotBlank
    @Size(min=8, max=10)
    private String password;
    

}
