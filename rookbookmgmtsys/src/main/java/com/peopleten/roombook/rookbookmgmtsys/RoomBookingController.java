package com.peopleten.roombook.rookbookmgmtsys;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

import com.peopleten.roombook.rookbookmgmtsys.model.BookingInformation;
import com.peopleten.roombook.rookbookmgmtsys.model.Customer;
import com.peopleten.roombook.rookbookmgmtsys.repository.BookingInfoRepository;
import com.peopleten.roombook.rookbookmgmtsys.repository.RoomBookRepo;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class RoomBookingController {
	
	@Autowired
	RoomBookRepo roomBookRepo;
	
	@Autowired
	BookingInfoRepository bookingInfoRepo;
	
	@GetMapping("/getallcustomer")
	public List<Customer> retrieveAllCustomers() {
		return roomBookRepo.findAll();
	}
	
	@GetMapping("/customers/{id}")
	public Customer retrieveCustomer(@PathVariable long id) {
		Optional<Customer> customer = roomBookRepo.findById(id);

		if (!customer.isPresent())
			throw new CustomerNotFoundException("id-" + id);

		return customer.get();
	}
	
	@PostMapping("/customers")
	public ResponseEntity<Object> createCustomer(@RequestBody Customer customer) {
		Customer savedCustomer = roomBookRepo.save(customer);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedCustomer.getCust_id()).toUri();

		return ResponseEntity.created(location).build();

	}
	
	@DeleteMapping("/customers/{id}")
	public void deleteCustomer(@PathVariable long id) {

		Optional<Customer> customer = roomBookRepo.findById(id);

		if (!customer.isPresent()) {
			throw new CustomerNotFoundException("id-" + id);
		} else {
			roomBookRepo.deleteById(id);
		}
	}
	
	@PutMapping("/customers/{id}")
	public ResponseEntity<Object> updateCustomer(@RequestBody Customer customer, @PathVariable long cust_id) {

		Optional<Customer> studentOptional = roomBookRepo.findById(cust_id);

		if (!studentOptional.isPresent())
			return ResponseEntity.notFound().build();

		customer.setCust_id(cust_id);
		
		roomBookRepo.save(customer);

		return ResponseEntity.noContent().build();
	}
	
	
	@GetMapping("/getallbookings")
	public List<BookingInformation> retrieveAllBookedRooms() {
		return bookingInfoRepo.findAll();
	}
	
	@PostMapping("/makebooking")
	public ResponseEntity<Object> createBooking(@RequestBody BookingInformation bookingInfo) {
		BookingInformation savedBooking = bookingInfoRepo.save(bookingInfo);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedBooking.getBookingId()).toUri();

		return ResponseEntity.created(location).build();

	}
	
	
	

}
