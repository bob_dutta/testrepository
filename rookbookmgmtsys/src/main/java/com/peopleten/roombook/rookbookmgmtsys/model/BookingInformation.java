package com.peopleten.roombook.rookbookmgmtsys.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class BookingInformation {

	
	@Id
	@GeneratedValue
    private Long bookingId;

	@NotBlank
    private String custEmail;
	
    private Date bookFromDate;
	
    private Date bookToDate;
	
	public BookingInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BookingInformation(Long bookingId, @NotBlank String custEmail, @NotBlank Date bookFromDate,
			@NotBlank Date bookToDate, @NotBlank String bookRoomNo, @NotBlank String custValidId) {
		super();
		this.bookingId = bookingId;
		this.custEmail = custEmail;
		this.bookFromDate = bookFromDate;
		this.bookToDate = bookToDate;
		this.bookRoomNo = bookRoomNo;
		this.custValidId = custValidId;
	}

	@NotBlank
    private String bookRoomNo;
	
	@NotBlank
    private String custValidId;

	public Long getBookingId() {
		return bookingId;
	}

	public void setBookingId(Long bookingId) {
		this.bookingId = bookingId;
	}

	public String getCustEmail() {
		return custEmail;
	}

	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}

	public Date getBookFromDate() {
		return bookFromDate;
	}

	public void setBookFromDate(Date bookFromDate) {
		this.bookFromDate = bookFromDate;
	}

	public Date getBookToDate() {
		return bookToDate;
	}

	public void setBookToDate(Date bookToDate) {
		this.bookToDate = bookToDate;
	}

	public String getBookRoomNo() {
		return bookRoomNo;
	}

	public void setBookRoomNo(String bookRoomNo) {
		this.bookRoomNo = bookRoomNo;
	}

	public String getCustValidId() {
		return custValidId;
	}

	public void setCustValidId(String custValidId) {
		this.custValidId = custValidId;
	}

	
}
