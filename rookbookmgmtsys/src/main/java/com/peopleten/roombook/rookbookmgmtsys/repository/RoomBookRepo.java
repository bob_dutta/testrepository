package com.peopleten.roombook.rookbookmgmtsys.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.peopleten.roombook.rookbookmgmtsys.model.Customer;

@Repository
public interface RoomBookRepo extends JpaRepository<Customer, Long>{

}
