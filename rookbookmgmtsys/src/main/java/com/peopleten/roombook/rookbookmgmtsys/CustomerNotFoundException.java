package com.peopleten.roombook.rookbookmgmtsys;


public class CustomerNotFoundException extends RuntimeException {

	public CustomerNotFoundException(String exception) {
		super(exception);
	}

}