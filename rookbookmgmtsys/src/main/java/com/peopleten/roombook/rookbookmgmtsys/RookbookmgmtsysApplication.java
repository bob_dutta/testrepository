package com.peopleten.roombook.rookbookmgmtsys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RookbookmgmtsysApplication {

	public static void main(String[] args) {
		SpringApplication.run(RookbookmgmtsysApplication.class, args);
	}

}
