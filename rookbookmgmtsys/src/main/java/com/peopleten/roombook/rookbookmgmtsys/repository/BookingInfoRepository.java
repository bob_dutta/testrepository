package com.peopleten.roombook.rookbookmgmtsys.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.peopleten.roombook.rookbookmgmtsys.model.BookingInformation;

@Repository
public interface BookingInfoRepository extends JpaRepository<BookingInformation, Long>{

}